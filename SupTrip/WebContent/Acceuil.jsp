<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Trip List</title>
</head>
<body>
<c:choose>
<c:when test="${empty idBooster}"><%@ include file="/login.jsp" %></c:when>
<c:otherwise><%@ include file="/home.jsp" %></c:otherwise>
</c:choose>

<!--<c:import url="/Login"></c:import>-->
<h4>Filters ma gueulle@Bitches marche plz</h4>

<form  action="/SupTrip/Acceuil" method="POST">
<table>

<tr>
	<th>Campus Name</th>
	<th>Start Date</th>
	<th>End DAte </th>
	
</tr>
<tr>	
	<td height="20">
		<input id="campus_name" name="campus_name"  type="text" />
	</td>
	<td>
		<input name="date1F" type="date" required="required" width="25%" />
	</td>
	<td>
		<input name="date2F" type="date" required="required" width="25%" />
	</td>	
</tr>
<tr>
	<td><input type="submit" value="Valider" /></td>
	<td><a href="/SupTrip/Acceuil"> <input type="button" value="Delete filters" /></a></td>
</tr>
</table>
</form>

<h3>stats</h3>

<c:if test="${!empty nbUser}">
<table>
<tr>
	<th>Number of users</th>
	<th>Number of trips</th>
</tr>
<tr>
	<th>${nbUser}</th>
	<th>${nbTrip}</th>
</tr>		
</table>
</c:if>
<h3>Trip List</h3>


<c:if test="${!empty listTrip}">
    <table class="tg">
    <tr>
        <th width="80">Campus Name</th>
        <th width="120">Start Place</th>
        <th width="120">Destination Place</th>
        <th width="120">Airport Name</th>
        <th width="120">Start Date </th>
        <th width="120">End Date </th>
        <th width="120">Price</th>
        <th width="60">Ticket</th>
        <th width="60">BAG</th>
    </tr>
   
   <c:forEach items="${listTrip}" var="trip"> 
           <tr>
            <td>${trip.campus_name}</td>
            <td>${trip.start_place}</td>
            <td>${trip.destination_place}></td>
            <td>${trip.airport_name}</td>
            <td>${trip.start_date}</td>
            <td>${trip.end_date}</td>
            <td>${trip.price}</td>
            <td>${trip.tickets}</td>
            <td>BAG</td>
        </tr>
    </c:forEach> 
    </table>
</c:if>

</body>
</html>