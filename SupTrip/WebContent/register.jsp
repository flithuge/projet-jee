<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
</head>
<script type="text/javascript">function capitalize(textboxid, str) {
    // string with alteast one character
    if (str && str.length >= 1)
    {       
        var firstChar = str.charAt(0);
        var remainingStr = str.slice(1);
        str = firstChar.toUpperCase() + remainingStr;
    }
    document.getElementById(textboxid).value = str;
}</script>
<body>
<h3>Provide all the fields for registration.</h3>
<form action="Register" method="post">
<strong>ID Booster</strong>:<input type="text" name="idbooster"><br>
<strong>Campus name</strong>:<input type="text" name="campusname" onkeyup="javascript:capitalize(this.id, this.value);"><br>
<strong>Firstname</strong>:<input type="text" name="firstname" onkeyup="javascript:capitalize(this.id, this.value);"><br>
<strong>Lastname</strong>:<input type="text" name="lastname" onkeyup="javascript:capitalize(this.id, this.value);"><br>
<strong>Password</strong>:<input type="password" name="password"><br>
<strong>email</strong>:<input type="text" name="email"><br>
<input type="submit" value="Register">
</form>
<br>
If you are registered user, please <a href="/login">login</a>.
</body>
</html>