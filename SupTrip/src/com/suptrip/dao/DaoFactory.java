package com.suptrip.dao;

import com.suptrip.doa.jpa.JpaTripDao;
import com.suptrip.doa.jpa.JpaUserDao;
import com.suptrip.util.PersistenceManager;

public class DaoFactory {
	private static JpaTripDao tripService;
	private static JpaUserDao userService;

	
	
	public static UserDao getUserDao() {
		if (userService == null) {
			userService = new JpaUserDao(PersistenceManager.getEntityManagerFactory());
		}
		return userService;
	}

	public static TripDao getTripDao() {
		if (tripService == null) {
			tripService = new JpaTripDao(PersistenceManager.getEntityManagerFactory());
		}
		return tripService;
	}
}
