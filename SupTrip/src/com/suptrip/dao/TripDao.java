package com.suptrip.dao;

import java.util.List;

import com.sutrip.models.Trip;

public interface TripDao {
	
	void addTrip(Trip t);
	void updateTrip(Trip t);
	void deleteTrip(Trip t);
	List<Trip> getAllTrip();
	List<Trip> getTripByCampusName(String campusname);
	List<Trip> getTripByDateAndCampusName(String campusname, String dateStart, String dateEnd);
	int getNbTrip(List<Trip> t);
	

}
