package com.suptrip.dao;

import java.util.List;

import com.sutrip.models.User;

public interface UserDao {
	
	void addUser(User u);
	void updateUser(User u);
	void deleteUser(User u);
	List<User> getAllUsers();
	int nbUsers(List<User> u);
	

}
