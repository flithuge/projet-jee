package com.suptrip.doa.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.suptrip.dao.UserDao;
import com.sutrip.models.Trip;
import com.sutrip.models.User;

public class JpaUserDao implements UserDao{
	
	private EntityManagerFactory emf;
	
	
	public JpaUserDao(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	
	@Override
	public void addUser(User u) {
		// TODO Auto-generated method stub
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		try {
			em.persist(u);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		
	}

	@Override
	public void updateUser(User u) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		try {
			em.merge(u);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public void deleteUser(User u) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		try {
			em.remove(em.merge(u));
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers(){
		EntityManager em = emf.createEntityManager();
		try {
			
			Query query = em.createQuery("from User");
			System.out.println(query.getResultList());
			return (List<User>)query.getResultList();
		} finally {
			em.close();
		}
	}
	@Override
	public int nbUsers(List<User> u){
		
		return u.size();
	}

}
