package com.suptrip.doa.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.suptrip.dao.TripDao;
import com.sutrip.models.Trip;

public class JpaTripDao implements TripDao{
	
	private EntityManagerFactory emf;

	public JpaTripDao(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public JpaTripDao() {
		// TODO Auto-generated constructor stub
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public void addTrip(Trip t) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		try {
			em.persist(t);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		
	}

	@Override
	public void updateTrip(Trip t) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		try {
			em.merge(t);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public void deleteTrip(Trip t) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		try {
			em.remove(em.merge(t));
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Trip> getAllTrip() {
		// TODO Auto-generated method stub
		
		EntityManager em = emf.createEntityManager();
		try {
			
			Query query = em.createQuery("from Trip");
			return (List<Trip>)query.getResultList();
		} finally {
			em.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Trip> getTripByCampusName(String campusname){
		
		EntityManager em = emf.createEntityManager();
		try{
			Query query = em.createQuery("From Trip WHERE campus_name='"+campusname+"'");
			System.out.println("From Trip WHERE campus_name='"+campusname+"'");
			return (List<Trip>)query.getResultList();
		} finally{
			em.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Trip> getTripByDateAndCampusName(String campusname,String dateStart,String dateEnd){
		
		EntityManager em  = emf.createEntityManager();
		try{
			Query query = em.createQuery("FROM Trip WHERE campus_name='"+campusname+"' AND start_date >='"+dateStart+"' AND end_date<='"+dateEnd+"'");
			return (List<Trip>)query.getResultList();
		} finally{
			em.close();
	}
		
	}	
	
	@Override
	public int getNbTrip(List<Trip> t){
				
		return t.size();
	}
	
	
	

}
