package com.suptrip.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sun.istack.internal.logging.Logger;
import com.suptrip.servlet.LoginServlet;

public class LogMysql {

	 static Logger logger = Logger.getLogger(LoginServlet.class);
	 public static boolean validUser(String idBooster,String password) 
     {
      boolean st =false;
      try{

	 //loading drivers for mysql
         Class.forName("com.mysql.jdbc.Driver");

 	 //creating connection with the database 
         Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/suptrip","root","");
         PreparedStatement ps =con.prepareStatement("select * from users where idbooster=? and password=?");
         ps.setString(1, idBooster);
         ps.setString(2, password);
         ResultSet rs = ps.executeQuery();
         logger.info(ps.toString());
         st = rs.next();
        
      }catch(Exception e)
      {
          e.printStackTrace();
      }
         return st;                 
  }  
	 public static void addUser(String idBooster,String firstName,String lastName,String campusName,String email, String password){
		 
		 
	      try{

		 //loading drivers for mysql
	         Class.forName("com.mysql.jdbc.Driver");

	 	 //creating connection with the database 
	         Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/suptrip","root","");
	         PreparedStatement ps =con.prepareStatement("insert into Users(idbooster,firstname,lastname,campusname,email,password,level) values (?,?,?,?,?,?,0)");
	         ps.setString(1, idBooster);
				ps.setString(2, firstName);
				ps.setString(3, lastName);
				ps.setString(4, campusName);
				ps.setString(5, email);
				ps.setString(6, password);
	         ps.execute();
	         logger.info(ps.toString());
	         ps.close();
	         con.close();
	        
	      }catch(Exception e)
	      {
	          e.printStackTrace();
	      }
	      
		 
	 }
}
