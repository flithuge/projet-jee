package com.suptrip.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.suptrip.util.LogMysql;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	 /**
	 * 
	 */
	Logger logger = Logger.getLogger(LoginServlet.class);
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        response.setContentType("text/html;charset=UTF-8");
	        PrintWriter out = response.getWriter();
	       
			
	        
	        String idBooster = request.getParameter("idBooster");
	        String password = request.getParameter("password");
	        logger.error(idBooster);
	        logger.error(password);
	        if(LogMysql.validUser(idBooster, password))
	        {
	           // RequestDispatcher rs = request.getRequestDispatcher("/Acceuil.jsp");
	            HttpSession session = request.getSession();
				session.setAttribute("idBooster", idBooster);
				
				request.setAttribute("idBooster", idBooster);
	            //rs.forward(request, response);
	            response.sendRedirect("Acceuil");
	        }
	        else
	        {
	        	HttpSession session = request.getSession();
				session.setAttribute("erreurLog", "Username or Password incorrect");
				//request.setAttribute("erreurLog", "Username or Password incorrect");
				response.sendRedirect("Acceuil");
	          
	        }
	    }  

}
