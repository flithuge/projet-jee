package com.suptrip.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.istack.internal.logging.Logger;
import com.sun.research.ws.wadl.Request;
import com.sun.xml.xsom.util.DomAnnotationParserFactory;
import com.suptrip.dao.DaoFactory;
import com.suptrip.dao.TripDao;
import com.suptrip.doa.jpa.JpaTripDao;
import com.sutrip.models.Trip;
import com.sutrip.models.User;

/**
 * Servlet implementation class Acceuil
 */
@WebServlet("/Acceuil")
public class Acceuil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 static Logger logger = Logger.getLogger(LoginServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Acceuil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
/*    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("listTrip",DaoFactory.getTripDao().getAllTrip());
		RequestDispatcher rd = request.getRequestDispatcher("/Acceuil.jsp");
		rd.forward(request, response);
		
	}*/
	
    
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String campus_name = request.getParameter("campus_name");
		String date1 = request.getParameter("date1F");
		String date2 = request.getParameter("date2F");
		System.out.println(campus_name);
		request.setAttribute("listTrip", DaoFactory.getTripDao().getTripByDateAndCampusName(campus_name, date1, date2));
		RequestDispatcher rd = request.getRequestDispatcher("/Acceuil.jsp");
		rd.forward(request, response);
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub 
		List<Trip> t = DaoFactory.getTripDao().getAllTrip() ;
		List<User> u = DaoFactory.getUserDao().getAllUsers();
		request.setAttribute("nbUser", DaoFactory.getUserDao().nbUsers(u));
		System.out.println(u);
		
		request.setAttribute("listTrip",t);
		request.setAttribute("nbTrip", DaoFactory.getTripDao().getNbTrip(t));
		RequestDispatcher rd = request.getRequestDispatcher("/Acceuil.jsp");
		rd.forward(request, response);
	}

	

}
